# Boiler plate
- This demo is maintained by poetry python. To run this project simply install poetry for python 3 like so:
```bash
 curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3
 ```
 - also dont forget to change the first line of `$HOME/.poetry/bin/poetry` from `#!/usr/bin/env python` to `#!/usr/bin/env python3`
 - to run the project do:
 ```
 poetry run uvicorn main:app --reload  
 ```